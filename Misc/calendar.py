import unittest
from copy import deepcopy

default_timerange = [(9, 00),  (17, 00)]  # Default hours are 9 - 5. Tuple has hours then minutes

class User:
    def __init__(self, id, name):
        self.name = name
        self.id = id

class Calendar:
    def __init__(self, user):
        self.days = {
            1: 'Sunday',
            2: 'Monday',
            3: 'Tuesday',
            4: 'Wednesday',
            5: 'Thursday',
            6: 'Friday',
            7: 'Saturday',
        }

        self.user = user

        self.events = [[] for x in range(7)]

    def get_day(self, day):
        return self.days[day]

    def add_event(self, day, start, end):
        self.events[day].append((start, end))

    def add_events_day(self, day, events):
        """
        Just because I am lazy
        :param day:
        :param events:
        :return:
        """
        self.events[day] = events

    def get_events_day(self, day):
        return self.events[day]


def available_week(week_calendars, duration, timerange=deepcopy(default_timerange)):

    avail = [ { 'day': x, 'avail': []} for x in range(7)]

    # Just work days
    for x in range(1, 6):
        dates = available_day(week_calendars, duration, x, timerange=timerange)
        avail[x]['avail'] = dates

    return avail


def available_day(days_calendars, duration, day, timerange=deepcopy(default_timerange)):
    """
    Get all available times for a specific day. Will check if all
    calendars are free for that duration
    :param days_calendars:
    :param duration:
    :param timerange:
    :return:
    """
    free_slots = []
    all_times = build_timerange(duration, timerange=timerange)

    def get_time_as_int(start, end):
        return start * 100 + end

    # While we are in the current available times (work times)
    for c_range in all_times:
        all_free = True
        # Look through all calendars
        for p_cal in days_calendars:
            events = p_cal.get_events_day(day)
            # Find all events. We start at 1 because we check to see if this duration will not overlap
            for j in range(1, len(events)):
                pre_event = events[j - 1]
                event = events[j]

                p_start = get_time_as_int(*pre_event[0])
                p_end = get_time_as_int(*pre_event[1])

                start_int = get_time_as_int(*event[0])
                end_int = get_time_as_int(*event[1])

                range_int = c_range[0] * 100 + c_range[1]
                next_c = add_duration(duration, *c_range)
                next_range = get_time_as_int(*next_c)

                if p_start <= range_int < p_end:
                    all_free = False
                    break

                if next_range > start_int and next_range <= end_int:
                    all_free = False
                    break

            if not all_free:
                break

        if all_free:
            free_slots.append(c_range)

    return free_slots


# Utils
def build_timerange(duration, timerange=deepcopy(default_timerange)):
    """
    Builds all possible ranges for that duration of time
    :param duration:
    :param timerange:
    :return:
    """
    current_timerange = timerange[0]
    end_timerange = timerange[1]

    all_ranges = [current_timerange]
    while current_timerange[0] < end_timerange[0]:
        new_duration = add_duration(duration, *current_timerange)
        all_ranges.append(new_duration)
        current_timerange = new_duration

    return all_ranges

def add_duration(duration, hours, minutes):
    """
    Get new duration. Accounts for 60 minutes in an hour
    :param duration:
    :param hours:
    :param minutes:
    :return:
    """
    new_mins = minutes + duration
    new_hours = hours
    while new_mins >= 60:
        new_mins -= 60
        new_hours += 1

    return new_hours, new_mins


class Test(unittest.TestCase):

    def setUp(self):
        user1 = User(1, 'Jason')
        user2 = User(3, 'John')

        self.calendar1 = Calendar(user1)
        self.calendar2 = Calendar(user2)

        self.calendar1.add_events_day(1, [((9, 00), (9, 15)), ((10, 0), (11, 0))])
        self.calendar1.add_events_day(2, [((9, 00), (9, 15)), ((10, 0), (11, 0))])

        self.calendar2.add_events_day(1, [((9, 30), (11, 0)), ((11, 30), (13, 0)), ((13, 30), (14, 0))])
        self.calendar2.add_events_day(2, [((9, 30), (11, 0)), ((11, 30), (13, 0)), ((13, 30), (14, 0))])

        self.tr = [(9, 00),  (14, 00)]

    def test_add_duration(self):
        self.assertEqual(add_duration(20, *(1, 30)), (1, 50))
        self.assertEqual(add_duration(20, *(1, 50)), (2, 10))
        self.assertEqual(add_duration(120, *(1, 20)), (3, 20))

    def test_build_timerange(self):
        tr = [(9, 00),  (10, 00)]
        expected = [(9, 00), (9, 30), (10, 00)]

        self.assertEqual(build_timerange(30, tr), expected)

        expected = [(9, 00), (9, 15), (9, 30), (9, 45), (10, 00)]
        self.assertEqual(build_timerange(15, tr), expected)

    def test_available_day(self):
        calendars = [self.calendar1]

        expected = [(9, 30), (11, 0), (11, 30), (12, 0), (12, 30), (13, 0), (13, 30), (14, 0)]
        free = available_day(calendars, 30, 1, timerange=self.tr)
        self.assertEqual(free, expected)
        calendars = [self.calendar1, self.calendar2]
        free = available_day(calendars, 30, 1, timerange=self.tr)
        expected = [(11, 0), (13, 0), (14, 0)]
        self.assertEqual(free, expected)

    def test_get_available_week(self):
        calendars = [self.calendar1]
        free = available_week(calendars, 30, timerange=self.tr)
        expected = [{'day': 0, 'avail': []}, {'day': 1, 'avail': [(9, 30), (11, 0), (11, 30), (12, 0), (12, 30), (13, 0), (13, 30), (14, 0)]}, {'day': 2, 'avail': [(9, 30), (11, 0), (11, 30), (12, 0), (12, 30), (13, 0), (13, 30), (14, 0)]}, {'day': 3, 'avail': [(9, 0), (9, 30), (10, 0), (10, 30), (11, 0), (11, 30), (12, 0), (12, 30), (13, 0), (13, 30), (14, 0)]}, {'day': 4, 'avail': [(9, 0), (9, 30), (10, 0), (10, 30), (11, 0), (11, 30), (12, 0), (12, 30), (13, 0), (13, 30), (14, 0)]}, {'day': 5, 'avail': [(9, 0), (9, 30), (10, 0), (10, 30), (11, 0), (11, 30), (12, 0), (12, 30), (13, 0), (13, 30), (14, 0)]}, {'day': 6, 'avail': []}]
        self.assertEqual(free, expected)

