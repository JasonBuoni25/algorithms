import unittest


def fish_simulation(fishes, years):
    """
    Run simulation
    :param fishes:
    :param: years to ru
    :return: fish array updated to account for simulation
    """

    current_year = 0

    total_fish = sum(fishes)
    total_reproductive = sum(fishes[5:])


    while current_year < years:
        babies = 0
        # Run sim
        # We care about > 1, 1, 3, 5, 99
        for fish_idx in range(len(fishes) - 1):
            if fish_idx == 0:
                # 99% mortality
                fishes[fish_idx] = int(fishes[fish_idx] * .01)
                pass
            if 0 < fish_idx < 3:
                # 90% mortality
                fishes[fish_idx] = int(fishes[fish_idx] * .1)
                pass
            if 3 <= fish_idx < 99:
                # 25% mortality
                fishes[fish_idx] = int(fishes[fish_idx] * .75)
                if fish_idx >= 5:
                    # We reproduce
                    babies += int(fishes[fish_idx] * 200)

        # Add babies and kill everyone over 99
        fishes_copy = [0] * 101
        fishes_copy[1:] = fishes
        fishes = fishes_copy[:100]
        fishes[0] = babies

        total_fish = sum(fishes)
        total_reproductive = sum(fishes[5:])
        current_year += 1

    return fishes, total_fish, total_reproductive


class Test(unittest.TestCase):
    def test_fish_simulation(self):
        fishes = [0] * 100
        fishes[0] = 100
        fishes, total_fish, total_reproductive = fish_simulation(fishes, 1)

        self.assertEqual(total_fish, 1)
        self.assertEqual(total_reproductive, 0)

        fishes = [0] * 100
        fishes[0] = 1000
        fishes, total_fish, total_reproductive = fish_simulation(fishes, 2)

        self.assertEqual(total_fish, 1)
        self.assertEqual(total_reproductive, 0)


class Square:
    def __init__(self, character, start_x, end_x, start_y, end_y):
        self.character = character
        self.start_x = start_x
        self.end_x = end_x
        self.start_y = start_y
        self.end_y = end_y


def create_grid(input):
    grid = []
    lines = input.split(' ')

    for line in lines:
        grid.append(list(line))

    return grid


def get_squares(grid):
    """

    :param grid: 2d table
    :return:
    """

    '''
     Go through entire grid
     Find character at position 
     Search for match in row
        if match, then get range
        seach y for character match at range
        if all true then square
        
    '''
    squares = []
    grid_height = len(grid)
    for top_idx in range(len(grid)):
        # Get the line
        line = grid[top_idx]

        for row_left, current_character in enumerate(line):
            # Iterate through the rest of the line to find a match
            for row_right in range(row_left + 1, len(line)):
                if current_character == line[row_right]:
                    # Found a match
                    distance = row_right - row_left

                    if (top_idx + distance) < grid_height \
                        and grid[top_idx + distance][row_left] == current_character \
                        and grid[top_idx + distance][row_right] == current_character:
                        # match found
                        found_square = Square(current_character, row_left, row_right, row_left + distance, row_right + distance)
                        squares.append(found_square)

    return squares


def get_squares_delta(grid):
    """

    :param grid: 2d table
    :return:
    """

    '''
     Go through entire grid
     Find character at position 
     Search for match in row
        if match, then get range
        seach y for character match at range
        if all true then square

    '''
    squares = []
    grid_height = len(grid)
    for top_idx in range(len(grid)):
        # Get the line
        line = grid[top_idx]

        # Bounding box
        for row_left in range(len(line)):
            # Iterate through the rest of the line to find a match
            for row_right in range(row_left + 1, len(line)):
                side_length = row_right - row_left
                for delta in range(side_length - 1):
                    current_character = grid[top_idx][row_left + delta]

                    bottom_idx = top_idx + side_length
                    if bottom_idx < grid_height:

                        top_right_character = grid[top_idx + delta][row_right]
                        bottom_left_character = grid[bottom_idx - delta][row_right]
                        bottom_right_character = grid[bottom_idx][row_right - delta]

                        if current_character == top_right_character == bottom_right_character == bottom_left_character:
                            found_square = Square(current_character, row_left, row_right, row_left + side_length,
                                                  row_right + side_length)
                            squares.append(found_square)


class LineItem:
    def __init__(self, name, *args):

        if name == 'hline' or name == 'vline':
            character, fixed, start, end = args
            if start > end:
                start, end = end, start

            self.name = name
            self.character = character
            self.fixed = fixed
            self.start = start
            self.end = end

        elif name == 'stamp':
            x_offset, y_offset = args
            self.x_offset = x_offset
            self.y_offset = y_offset

class Stamp:
    def __init__(self, name, width, height):
        self.name = name
        self.s = [['.' for x in range(width)] for y in range(height)]

    def draw(self, line_list):
        """
        :param line_list: Will be of type LineItem
        :return:
        """
        for line in line_list:
            if line.name == 'hline':
                # Find start position
                for x in range(line.start, line.end + 1):
                   self.s[line.fixed][x] = line.character
            elif line.name == 'vline':
                # Iterate
                for y in range(line.start, line.end + 1):
                    self.s[y][line.fixed] = line.character

        return self.s


class Drawing:

    def __init__(self, width, height, stamps):
        """

        :param width:
        :param height:
        :param stamps: hash table of all stamps
        """
        self.t = [['.' for x in range(width)] for y in range(height)]
        self.stamps = stamps

    def draw(self, line_list):
        """
        :param line_list: Will be of type LineItem
        :return:
        """
        for line in line_list:
            if line.name == 'hline':
                # Find start position
                for x in range(line.start, line.end + 1):
                   self.t[line.fixed][x] = line.character
            elif line.name == 'vline':
                # Iterate
                for y in range(line.start, line.end + 1):
                    self.t[y][line.fixed] = line.character
            elif line.name ==  'stamp':
                # stamp ${name} {top-left-x} {top-left-y}
                if line.name in self.stamps:
                    stamp = self.stamps[line.name].s

                    for i in range(len(stamp)):
                        for j in range(len(stamp[i])):
                            self.t[i + line.offset_y][j + line.offset_x] = stamp[i][j]

        return self.t

def do_drawing(i_string):
    """
        Ex:
        input1 = drawing 15 12
                    hline A 3 2 10
                    vline B 6 7 1
    :param i_string:
    :return:
    """
    input_line = i_string.split('\n')
    drawing = None
    line_list = []

    for idx, i_line in enumerate(input_line):
        if idx == 0:
            single_line = i_line.split(' ')
            height = int(single_line[2])
            width = int(single_line[1])
            drawing = Drawing(width, height)
        else:
            single_line = i_line.split(' ')
            name, character, fixed, start, end = single_line
            line_item = LineItem(name, character, int(fixed), int(start), int(end))
            line_list.append(line_item)

    return drawing.draw(line_list)